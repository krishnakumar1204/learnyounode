const fs = require('fs');

fs.readdir(process.argv[2], (err, fileList) => {
    if(err){
        console.log(err);
    }
    else{
        const filesWithExtension = fileList.filter((fileName) => fileName.endsWith(`.${process.argv[3]}`));
        filesWithExtension.forEach((file) => {
            console.log(file);
        })
    }
})