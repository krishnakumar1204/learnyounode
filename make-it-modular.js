const getFilesWithExtension = require('./mymodule.js');

getFilesWithExtension(process.argv[2], process.argv[3], (err, filesWithExtension) => {
    if(err){
        console.log(err);
    }
    else{
        filesWithExtension.forEach(file => {
            console.log(file);
        });
    }
})