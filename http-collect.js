const http = require('http');

http.get(process.argv[2], (response) => {
    let content = '';

    response.on('data', (chunk) => {
        content += chunk;
    });

    response.on('end', () => {
        console.log(content.length);
        console.log(content);
    })
})
.on('error', (err) => {
    console.log(err);
})