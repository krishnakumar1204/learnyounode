const http = require('http');

let contents = [];
let cnt=0;

for(let i=0; i<3; i++){
    http.get(process.argv[2+i], (res) => {

        res.setEncoding('utf-8');

        let content = '';

        res.on('data', (chunk) => {
            content += chunk;
        })
        

        res.on('end', () => {
            contents[i] = content;
            cnt++;

            if(cnt === 3){
                contents.forEach((content) => {
                    console.log(content);
                })
            }
        })
    })
    .on('error', (err) => {
        console.log("err", err);
    })
    
}