const http = require('http');

const server = http.createServer((req, res) => {
    if (req.method === 'GET') {

        const URL = req.url;

        let time = URL.split('iso=')[1];
        let date = new Date(time);

        if (URL.startsWith('/api/parsetime')) {


            let timeObj = {
                "hour": date.getHours(),
                "minute": date.getMinutes(),
                "second": date.getSeconds()
            }

            res.end(JSON.stringify(timeObj));
        }
        else if(URL.startsWith('/api/unixtime')){
            let unixTime = {
                unixtime: date.getTime()
            }

            res.end(JSON.stringify(unixTime));
        }
    }
    else {
        res.end("Not a 'GET' request");
    }
})
    .on('error', console.error);

server.listen(process.argv[2]);