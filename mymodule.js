const fs = require('fs');

function getFilesWithExtension(dirPath, extension, callback){

    if((typeof dirPath === 'string' && dirPath !== '') && (typeof extension === 'string' && extension !== '') && (typeof callback === 'function')){
        
        fs.readdir(dirPath, (err, fileList) => {
            if(err){
                return callback(err);
            }
            else{
                const filesWithExtension = fileList.filter((fileName) => fileName.endsWith(`.${extension}`));
                callback(null, filesWithExtension);
            }
        });
    }
}

module.exports = getFilesWithExtension;