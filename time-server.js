const net = require('net');

const date = new Date();

const year = date.getFullYear();
const month = date.getMonth()+1;
const day = date.getDate();
const hrs = date.getHours();
const mins = date.getMinutes();

const currDate = year+'-'+fillZero(month)+'-'+fillZero(day)+' '+fillZero(hrs)+':'+fillZero(mins);

function fillZero(num){
    return (num<10)?('0'+num):num;
}



const server = net.createServer((socket) => {
    socket.write(currDate + '\n');
    socket.end();
})

server.listen(process.argv[2]);